Inscription à Slack
======================

Slack est une application de messagerie professionnelle qui facilite la collaboration et la communication entre les membres d'une équipe.
Elle permet de faciliter l'accès à l'information et aux échanges et limiter le nombre d'emails envoyés qui sont parfois non lus et perçu comme des spams car trop nombreux.

Ce tutoriel a pour objectif de montrer comment créer un compte sur Slack afin de rejoindre la communauté de travail spécifique à la DREES :
[drees-echange.slack.com](https://drees-echange.slack.com/signup) (anciennement _bigdatasante-social_ sur les captures d'écran).

Cette page permet de s'inscrire directement avec son email professionnel prenom.nom@sante.gouv.fr :

![1_slack_inscription](https://gitlab.com/DREES/tutoriels/raw/master/img/slack/1_slack_inscription.JPG)

Entrez votre email et cliquez sur "créer un compte". On obtient un message de confirmation sur la page suivante :

![2_slack_confirmation](https://gitlab.com/DREES/tutoriels/raw/master/img/slack/2_slack_confirmation.JPG)

Un email de confirmation est envoyé sur votre boîte email. Il faut cliquer sur le lien de validation dans l'email reçu. 
On vous demandera de choisir un mot de passe.

Vous pourrez ensuite vous connecter à partir de votre ordinateur avec la version web.
Si vous le souhaitez, vous pouvez aussi télécharger l'application sur votre ordinateur (https://slack.com/intl/fr-fr/downloads/windows) ou sur votre smartphone.
Pour vous connectez, il suffit d'indiquer votre email et mot de passe. 

![3_slack_connexion](https://gitlab.com/DREES/tutoriels/raw/master/img/slack/3_slack_connexion.JPG)

Vous aurez ensuite accès directement à l'espace d'échange avec vos collaborateurs :

![4_slack_interface](https://gitlab.com/DREES/tutoriels/raw/master/img/slack/4_slack_interface.JPG)

Voici le descriptif des differentes zones de l'interface :

![5_slack_interface](https://gitlab.com/DREES/tutoriels/raw/master/img/slack/5_slack_interface_legend.JPG)

1- La gestion de son compte : c’est en cliquant ici que l’on peut accéder au paramétrage de son profil, ses préférences, changer de groupe de travail etc.

2- Les channels : ici, on accède aux différents groupes de discussion, triés par thématique. Dans notre projet, nous avons créé un groupe en ce qui concerne le design, un autre en ce qui concerne l’intégration, ensuite un groupe plus général, les idées etc.

3- Les messages directs : si l’on veut échanger avec un membre de son équipe en direct, on peut directement accéder à son compte par ici

4- Inviter des personnes à rejoindre l’équipe : simplement en cliquant ici, une page s’ouvre et permet d’inviter les personnes à travers leur adresse e-mail

5- Pour créer un post : c’est ici que l’on interagit dans un groupe de discussion, on post un message, on télécharge un fichier, on créé un « Snippet » (c’est pour les développeurs qui souhaitent partager leur code)

6- Le fil d’actualité : directement lié au channel de discussion. Ici, c’est le channel « design » que nous visualisons, où l’on retrouve les différentes discussions au fil du temps

7- Une barre de recherche : très pratique et puissante, cette barre permet de retrouver des messages ou des fichiers. Elle permet de filtrer par les résultats les plus récents ou encore les plus pertinents

8- Un peu de détails : permet d’approfondir sa recherche par mention, fichiers, ou encore trouver l’aide

9- Un sous-menu : spécifique au groupe de discussion, il permet de gérer les informations liées au groupe dans lequel on se trouve


Références :
https://la-studio.fr/slack-outils-collaboration-communiquer-equipe/
Tuto vidéo de 2 min :
https://www.youtube.com/watch?v=v2eKsV_ecmI
